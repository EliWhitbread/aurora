﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SceneObjectIDSetter))]
public class SceneObjectIDSetEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        SceneObjectIDSetter idSetter = (SceneObjectIDSetter)target;
        if (GUILayout.Button("Set All IDs"))
        {
            idSetter.SetAllIDs();
        }
    }
}
