﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

public class SceneObjectIDSetter : MonoBehaviour
{
    public int startingID;
    [SerializeField] int count;
    [SerializeField] string message = "idle";

    public void SetAllIDs()
    {
#if UNITY_EDITOR
        message = "Precessing";
        int _id = startingID;
        count = 0;
        RawResourceItem[] items = GameObject.FindObjectsOfType<RawResourceItem>();
        count = items.Length;

        for (int i = 0; i < items.Length; i++)
        {
            GameObject _obj = items[i].gameObject;

            HarvestableResourceData _data = items[i].ResourceData;
            _data.id = _id + i;

            items[i].ResourceData = _data;
            PrefabUtility.RecordPrefabInstancePropertyModifications(_obj);
            EditorUtility.SetDirty(items[i]);
            
            message = items[i].gameObject.name;
        }

        EditorSceneManager.SaveScene(EditorSceneManager.GetActiveScene());

        message = "Done";
#endif
    }
}
