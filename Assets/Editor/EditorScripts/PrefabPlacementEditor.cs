﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(PrefabPlacement))]
public class PrefabPlacementEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PrefabPlacement prefabPlacement = (PrefabPlacement)target;
        if(GUILayout.Button("Place Prefabs"))
        {
            prefabPlacement.PlacePrefabs();
        }
    }
}
