﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSetup : MonoBehaviour
{
    [SerializeField] bool buildToFullScreen = false, lockCursor = true;
    int[] scenesToHideCursor = { 1 } ;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }

    private void SceneManager_sceneLoaded(Scene _scn, LoadSceneMode mode)
    {
        SetupGame(_scn.buildIndex);
    }

    void SetupGame(int _scnIndex)
    {
        if (buildToFullScreen == false)
        {
            Screen.fullScreenMode = FullScreenMode.FullScreenWindow;
            Screen.fullScreen = false;
        }
        if (lockCursor == true && HideCursor(_scnIndex) == true)
        {
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
    }

    bool HideCursor(int _scnIndex)
    {
        for (int i = 0; i < scenesToHideCursor.Length; i++)
        {
            if (_scnIndex == scenesToHideCursor[i])
            {
                return true;
            }            
        }

        return false;
    }
}
