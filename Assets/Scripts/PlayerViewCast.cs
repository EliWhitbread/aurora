﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerViewCast : MonoBehaviour
{
    [SerializeField] float interactionDistance;
    [SerializeField] LayerMask layersToIgnore;
    float lookCheckTimer, lookCastDelay = 0.1f;
    bool playerViewCastEnabled = true;
    IInteractable currentInteractableObject = null;
    PlayerInteract playerInteract;
    [SerializeField] Transform viewOrigin;

    PhotonView punView;

    // Start is called before the first frame update
    void Start()
    {
        playerInteract = gameObject.GetComponent<PlayerInteract>();
        //viewOrigin = this.transform.GetComponentInChildren<Camera>().transform;
        viewOrigin = gameObject.transform.Find("ViewGimbol").transform.Find("SceneCamera").transform;
        punView = gameObject.GetComponent<PhotonView>();
        if(punView.IsMine)
        {
            playerViewCastEnabled = true;
            StartCoroutine(PlayerViewRaycastActive());
        }
        
    }

    IEnumerator PlayerViewRaycastActive()
    {
        while (playerViewCastEnabled == true)
        {
            RaycastPlayerView();
            yield return new WaitForSeconds(lookCastDelay);
        }
    }

    void RaycastPlayerView()
    {

        RaycastHit hit;
        if (Physics.Raycast(viewOrigin.position, viewOrigin.forward, out hit, interactionDistance, ~layersToIgnore))
        {
            Debug.Log("Raycaster looking at: " + hit.collider.gameObject.name);
            IInteractable _interactable = hit.collider.gameObject.GetComponent<IInteractable>();
            if (_interactable != null)
            {
                if (currentInteractableObject != _interactable)
                {
                    if (currentInteractableObject != null)
                    {
                        currentInteractableObject.HasFocusStart();

                    }
                    currentInteractableObject = _interactable;
                    playerInteract.InteractableObject = _interactable;
                    currentInteractableObject.HasFocusStart();
                }

            }
        }
        else if (currentInteractableObject != null)
        {
            currentInteractableObject.HasFocusEnd();
            currentInteractableObject = null;
            playerInteract.InteractableObject = null;
        }
        
    }
}
