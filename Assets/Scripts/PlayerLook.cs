﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerLook : MonoBehaviour
{
    [SerializeField] Transform camTransform;
    [SerializeField] float lookSpeedX, lookSpeedY;

    [Header("Third Person")]
    [SerializeField] bool useThirdPersonCamera = false;
    [SerializeField] Transform camTarget;
    [SerializeField] float camFollowDistance, camFollowHeight, camLeftOffset;


    Rigidbody rBody;

    Vector2 mouseLookVector = Vector2.zero;

    //Pun
    PhotonView punView;

    private void OnEnable()
    {
        InputManager.OnKeyDown_Begin += InputManager_OnKeyDown_Begin;
    }

    private void OnDisable()
    {
        InputManager.OnKeyDown_Begin -= InputManager_OnKeyDown_Begin;
    }

    private void InputManager_OnKeyDown_Begin(KeyboardInput keyboardInput)
    {
        if (punView.IsMine)
        {
            if (keyboardInput == KeyboardInput.CAMERA_VIEW_SWITCH)
            {
                if(camTransform == null)
                {
                    SetUp();
                }

                camTarget.localRotation = Quaternion.identity;
                camTransform.localPosition = Vector3.zero;
                camTransform.localRotation = camTarget.localRotation;

                Quaternion _rot = camTransform.localRotation;
                _rot.y = 0.0f;                

                camTransform.localRotation = _rot;
                //camTransform.localPosition = Vector3.zero;

                useThirdPersonCamera = !useThirdPersonCamera;

                if (useThirdPersonCamera == true)
                {
                    Vector3 _newCamPos = camTransform.localPosition;
                    _newCamPos.z -= camFollowDistance;
                    _newCamPos.y -= camFollowHeight;
                    _newCamPos.x += camLeftOffset;
                    camTransform.localPosition = _newCamPos;

                }

            }
        }        
    }

    void Start()
    {
        SetUp();        
    }

    void SetUp()
    {
        punView = gameObject.GetComponent<PhotonView>();
        if (punView.IsMine)
        {
            camTransform = this.transform.GetComponentInChildren<Camera>().transform;
            rBody = gameObject.GetComponent<Rigidbody>();
            mouseLookVector = Vector2.zero;
            camTarget = gameObject.transform.Find("ViewGimbol");
            if (useThirdPersonCamera == true)
            {
                Vector3 _newCamPos = camTransform.position;
                _newCamPos.z -= camFollowDistance;
                _newCamPos.y += camFollowHeight;
                camTransform.position = _newCamPos;
            }
        }
        else
        {
            //gameObject.transform.GetChild(0).gameObject.SetActive(false);
            gameObject.transform.Find("ViewGimbol").gameObject.SetActive(false);
        }
    }

    void LateUpdate()
    {
        if(punView.IsMine)
        {
            mouseLookVector = new Vector2(Input.GetAxis("Mouse X") * lookSpeedX, Input.GetAxis("Mouse Y") * lookSpeedY);

            if (useThirdPersonCamera == true)
            {
                camTarget.Rotate(Vector3.left * (mouseLookVector.y * Time.deltaTime));

            }
            else
            {
                camTransform.Rotate(Vector3.left * (mouseLookVector.y * Time.deltaTime));
            }
                        
            Vector3 eulerAngle = Vector3.zero;
            eulerAngle.y = mouseLookVector.x;
            Quaternion deltaRot = Quaternion.Euler(eulerAngle * Time.deltaTime);
            rBody.MoveRotation(rBody.rotation * deltaRot);
            
            mouseLookVector = Vector2.zero;
        }
        
    }

}
