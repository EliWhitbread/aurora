﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInventory : MonoBehaviour
{
    [SerializeField] int availableWood, availableStone;
    [SerializeField] UIManager uIManager;

    public int AvailableWood
    {
        get { return availableWood; }
        set { availableWood = value; UpdateUIStats(UIDisplayItems.WOOD); }
    }

    public int AvailableStone
    {
        get { return availableStone; }
        set { availableStone = value; UpdateUIStats(UIDisplayItems.STONE); }
    }

    // Start is called before the first frame update
    void Start()
    {
        if(uIManager == null)
        {
            uIManager = GameObject.FindGameObjectWithTag("Canvas").transform.GetComponent<UIManager>();
        }
    }

    void UpdateUIStats(UIDisplayItems uiItem)
    {
        switch (uiItem)
        {
            case UIDisplayItems.NULL:
                //do nothing
                break;
            case UIDisplayItems.WOOD:
                uIManager.UpdateUI(uiItem, availableWood.ToString());
                break;
            case UIDisplayItems.STONE:
                uIManager.UpdateUI(uiItem, availableStone.ToString());
                break;
            default:
                break;
        }
    }
}
