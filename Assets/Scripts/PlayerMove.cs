﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerMove : MonoBehaviour
{
    CharacterController characterController;
    [SerializeField] float moveSpeed, runSpeed, jumpSpeed, gravity = 20.0f;

    Vector3 moveDirection = Vector3.zero;
    bool isRunning = false;
    Transform playerTransform;

    Animator anim;

    //Pun
    PhotonView punView;

    // Start is called before the first frame update
    void Start()
    {
        characterController = gameObject.GetComponent<CharacterController>();
        playerTransform = this.transform;
        punView = gameObject.GetComponent<PhotonView>();
        anim = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (punView.IsMine)
        {
            isRunning = Input.GetKey(KeyCode.LeftShift);

            //test input
            if (characterController.isGrounded == true)
            {
                moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
                moveDirection.z *= isRunning == true ? runSpeed : moveSpeed;
                moveDirection.x *= moveSpeed;

                if (Input.GetKey(KeyCode.Space))
                {
                    moveDirection.y = jumpSpeed;
                }
            }
            
            moveDirection.y -= gravity * Time.deltaTime;
            
            

            characterController.Move(playerTransform.TransformDirection(moveDirection) * Time.deltaTime);
            float animX = Mathf.Clamp(moveDirection.x, -1.0f, 1.0f);
            float animZ = Mathf.Clamp(moveDirection.z, -1.0f, 1.0f);
            //Vector3 _moveVector = characterController.velocity;
            anim.SetFloat("velocityX", animX);
            anim.SetFloat("velocityZ", animZ *= isRunning ? 1.0f : 0.3f);
        }
        
    }
}
