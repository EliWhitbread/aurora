﻿

public interface IInteractable
{
    void HasFocusStart();
    void HasFocusEnd();
    void InteractStart(PlayerInteract playerInteracting);
    void InteractEnd();
}

public interface IDamageable
{
    void TakeDamage(int amt);
}
