﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingImage : MonoBehaviour
{
    Image image;
    [SerializeField] float revealSpeed;

    float fillAmt = 0.0f;

    private void OnEnable()
    {
        if(image == null)
        {
            image = gameObject.GetComponent<Image>();
        }
    }

    void Update()
    {
        fillAmt += Time.deltaTime * revealSpeed;
        if(fillAmt >= 1.1f)
        {
            fillAmt = 0.0f;
        }

        image.fillAmount = Mathf.Clamp(fillAmt, 0.0f, 1.0f); 
    }
}
