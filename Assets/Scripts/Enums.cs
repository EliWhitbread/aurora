﻿
public enum RawResourceType { NULL, WOOD, STONE}

//INPUTS
//Mouse
public enum MouseInput { NULL, MOUSE_LEFT, MOUSE_RIGHT }
//Keyboard
public enum KeyboardInput { NULL, ESC, INTERACT_PRIMARY_E, JUMP, RUN, MOVE_FORWARD, MOVE_BACKWARD, MOVE_LEFT, MOVE_RIGHT, CAMERA_VIEW_SWITCH }
//Controller ??

public enum UIDisplayItems { NULL, WOOD, STONE }