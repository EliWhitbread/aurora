﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

public class PhotonLobby : MonoBehaviourPunCallbacks
{
    public static PhotonLobby photonLobby { get; private set; }

    [SerializeField] GameObject networkPlayButton, cancelButton, waitingText, waitingImage;
    [SerializeField] TextMeshProUGUI serverMessageText;

    int tryCount = 0;
    [SerializeField] bool showNetworkConnectionMessage = true;

    private void Awake()
    {
        if(photonLobby != null && photonLobby != this)
        {
            Destroy(this);
        }
        else
        {
            photonLobby = this;
        }
        showNetworkConnectionMessage = true;
    }

    private void Start()
    {
        networkPlayButton.SetActive(false);
        cancelButton.SetActive(false);
        waitingText.SetActive(true);
        waitingImage.SetActive(true);

        PhotonNetwork.ConnectUsingSettings();
    }

    private void FixedUpdate()
    {
        if(showNetworkConnectionMessage)
        {
            UpdateNetworkMessage();
        }        
    }

    void UpdateNetworkMessage()
    {
        serverMessageText.text = PhotonNetwork.NetworkClientState.ToString();
    }

    public override void OnConnectedToMaster()
    {
        Debug.Log("Player connected to master server!");
        PhotonNetwork.AutomaticallySyncScene = true;
        waitingText.SetActive(false);
        waitingImage.SetActive(false);
        networkPlayButton.SetActive(true);
        showNetworkConnectionMessage = false;
        UpdateNetworkMessage();
    }

    public void OnPlayButtonClicked()
    {
        showNetworkConnectionMessage = true;
        networkPlayButton.SetActive(false);
        cancelButton.SetActive(true);

        tryCount = 0;
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        Debug.Log("Failed to join random room! " + message);

        CreateRoom();
    }

    void CreateRoom()
    {
        int _rndRoomName = Random.Range(0, 10000);
        RoomOptions _options = new RoomOptions() { IsVisible = true, IsOpen = true, MaxPlayers = (byte)MultiplayerSettings.multiplayerSettings.MaxPlayers };
        PhotonNetwork.CreateRoom("Room" + _rndRoomName, _options);
    }

    public override void OnJoinedRoom()
    {
        UpdateNetworkMessage();
        showNetworkConnectionMessage = false;
        serverMessageText.text = PhotonNetwork.CurrentRoom.Name;
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        tryCount++;
        if(tryCount > 50)
        {
            //get outa here - not able to join or create a room.
            OnCancelButtonClick();
        }
        Debug.Log("Could not create room!" + message);
        CreateRoom();
    }

    public void OnCancelButtonClick()
    {
        cancelButton.SetActive(false);
        networkPlayButton.SetActive(true);
        PhotonNetwork.LeaveRoom();
        showNetworkConnectionMessage = true;
    }
}
