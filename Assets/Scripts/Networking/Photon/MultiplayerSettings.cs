﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplayerSettings : MonoBehaviour
{
    public static MultiplayerSettings multiplayerSettings { get; private set; }

    [SerializeField] bool delayStart = false;
    [SerializeField] int maxPlayers, menuScene, multiplayerScene;

    public bool DelayStart
    {
        get { return delayStart; }
    }

    public int MaxPlayers
    {
        get { return maxPlayers; }
    }

    public int MenuScene
    {
        get { return menuScene; }
    }

    public int MultiplayerScene
    {
        get { return multiplayerScene; }
    }

    private void Awake()
    {
        if(multiplayerSettings != null && multiplayerSettings != this)
        {
            Destroy(this);
        }
        else
        {
            multiplayerSettings = this;
        }
        DontDestroyOnLoad(this.gameObject);

    }

}
