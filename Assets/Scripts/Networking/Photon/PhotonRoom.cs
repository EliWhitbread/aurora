﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.SceneManagement;
using System;
using System.IO;

public class PhotonRoom : MonoBehaviourPunCallbacks, IInRoomCallbacks
{
    public static PhotonRoom photonRoom { get; private set; }

    //room info
    PhotonView punView;
    bool isGameLoaded = false;
    int currentScene;

    //players
    Player[] photonPlayers;
    int playersInRoom, thisPlayerNumberInRoom, playersInGame;

    //Dealyed start
    bool readyToCount, readyToStart;
    [SerializeField] float lessThanMaxPlayers, atMaxPlayers, timeToStart, startingTime;

    #region ..accessors
    public int PlayersInRoom
    {
        get { return playersInRoom; }
        set { playersInRoom = value; }
    }

    public int ThisPlayerNumberInRoom
    {
        get { return thisPlayerNumberInRoom; }
        set { thisPlayerNumberInRoom = value; }
    }

    public int PlayersInGame
    {
        get { return playersInGame; }
        set { playersInGame = value; }
    }

    public bool IsGameLoaded
    {
        get { return isGameLoaded; }
        set { isGameLoaded = value; }
    }

    public int CurrentScene
    {
        get { return currentScene; }
        set { currentScene = value; }
    }

    public float StartingTime
    {
        get { return startingTime; }
        set { startingTime = value; }
    }

#endregion

    private void Awake()
    {
        if(photonRoom != null && photonRoom != this)
        {
            Destroy(photonRoom.gameObject);
            photonRoom = this;
        }
        else
        {
            photonRoom = this;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public override void OnEnable()
    {
        base.OnEnable();
        PhotonNetwork.AddCallbackTarget(this);
        SceneManager.sceneLoaded += SceneManager_sceneLoaded;
    }

    private void SceneManager_sceneLoaded(Scene _scene, LoadSceneMode _mode)
    {
        CurrentScene = _scene.buildIndex;
        if(CurrentScene == MultiplayerSettings.multiplayerSettings.MultiplayerScene)
        {
            IsGameLoaded = true;

            if(MultiplayerSettings.multiplayerSettings.DelayStart == true)
            {
                punView.RPC("RPC_LoadedGameScene", RpcTarget.MasterClient);
            }
            else
            {
                RPC_CreatePlayer();
            }
        }
    }

    public override void OnDisable()
    {
        base.OnDisable();
        PhotonNetwork.RemoveCallbackTarget(this);
        SceneManager.sceneLoaded -= SceneManager_sceneLoaded;
    }

    void Start()
    {
        punView = gameObject.GetComponent<PhotonView>();
        readyToCount = false;
        readyToStart = false;
        lessThanMaxPlayers = StartingTime;
        atMaxPlayers = 6;
        timeToStart = StartingTime;
    }

    void Update()
    {
        if(MultiplayerSettings.multiplayerSettings.DelayStart == true)
        {
            if(PlayersInRoom == 1)
            {
                RestartTimer();
            }
            if(IsGameLoaded == false)
            {
                if(readyToStart == true)
                {
                    atMaxPlayers -= Time.deltaTime;
                    lessThanMaxPlayers = atMaxPlayers;
                    timeToStart = atMaxPlayers;
                }
                else if(readyToCount == true)
                {
                    lessThanMaxPlayers -= Time.deltaTime;
                    timeToStart = lessThanMaxPlayers;
                }
                if(timeToStart <= 0.0f)
                {
                    StartGame();
                }
            }
        }
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        photonPlayers = PhotonNetwork.PlayerList;
        PlayersInRoom = photonPlayers.Length;
        ThisPlayerNumberInRoom = PlayersInRoom;
        PhotonNetwork.NickName = ThisPlayerNumberInRoom.ToString();

        if(MultiplayerSettings.multiplayerSettings.DelayStart == true)
        {
            if(PlayersInRoom > 1)
            {
                readyToCount = true;
            }
            if(PlayersInRoom == MultiplayerSettings.multiplayerSettings.MaxPlayers)
            {
                readyToStart = true;
                if(PhotonNetwork.IsMasterClient == false)
                {
                    return;
                }
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }
        }
        else
        {
            StartGame();
        }
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        base.OnPlayerEnteredRoom(newPlayer);
        photonPlayers = PhotonNetwork.PlayerList;
        PlayersInRoom++;

        if(MultiplayerSettings.multiplayerSettings.DelayStart == true)
        {
            if(PlayersInRoom > 1)
            {
                readyToCount = true;
            }
            if(PlayersInRoom == MultiplayerSettings.multiplayerSettings.MaxPlayers)
            {
                readyToStart = true;
                if (PhotonNetwork.IsMasterClient != true)
                {
                    return;
                }
                PhotonNetwork.CurrentRoom.IsOpen = false;
            }
        }
    }

    private void StartGame()
    {
        IsGameLoaded = true;
        if(PhotonNetwork.IsMasterClient == false)
        {
            return;
        }
        if(MultiplayerSettings.multiplayerSettings.DelayStart == true)
        {
            PhotonNetwork.CurrentRoom.IsOpen = false;
        }
        PhotonNetwork.LoadLevel(MultiplayerSettings.multiplayerSettings.MultiplayerScene);
    }

    private void RestartTimer()
    {
        lessThanMaxPlayers = StartingTime;
        timeToStart = StartingTime;
        atMaxPlayers = 6;
        readyToCount = false;
        readyToStart = false;
    }

    [PunRPC]
    private void RPC_LoadedGameScene()
    {
        PlayersInGame++;
        if(PlayersInGame == PhotonNetwork.PlayerList.Length)
        {
            punView.RPC("RPC_CreatePlayer", RpcTarget.All);
        }

    }

    [PunRPC]
    private void RPC_CreatePlayer()
    {
        PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "PhotonNetworkPlayer"), transform.position, Quaternion.identity, 0); //player controller - NOT avatar. Position not relevant
    }
}
