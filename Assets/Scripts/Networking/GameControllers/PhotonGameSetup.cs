﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonGameSetup : MonoBehaviour
{
    public static PhotonGameSetup gameSetup { get; private set; }

    [SerializeField] Transform[] networkSpawnPoints;

    public Transform GetRandomSpawnPoint()
    {
        int _rndSP = Random.Range(0, networkSpawnPoints.Length);
        return networkSpawnPoints[_rndSP];
    }

    private void OnEnable()
    {
        if(gameSetup != null && gameSetup != this)
        {
            Destroy(this);
        }
        else
        {
            gameSetup = this;
        }
    }
}
