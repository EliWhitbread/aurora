﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using System.IO;

public class PhotonPlayer : MonoBehaviour
{
    PhotonView punView;
    [SerializeField] GameObject myAvatar;

    void Start()
    {
        punView = gameObject.GetComponent<PhotonView>();
        if(punView.IsMine)
        {
            Transform _t = PhotonGameSetup.gameSetup.GetRandomSpawnPoint();
            myAvatar = PhotonNetwork.Instantiate(Path.Combine("PhotonPrefabs", "Alia_01"), _t.position, _t.rotation, 0);
            Rigidbody _rBody = myAvatar.GetComponent<Rigidbody>();
            _rBody.isKinematic = false;
        }
    }

}
