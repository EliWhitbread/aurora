﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI woodText, stoneText, testResourceObject_text;

    private void Awake()
    {
        if(woodText == null)
        {
            woodText = gameObject.transform.Find("Wood Display").GetComponent<TextMeshProUGUI>();
        }
        if(stoneText == null)
        {
            stoneText = gameObject.transform.Find("Stone Display").GetComponent<TextMeshProUGUI>();
        }

        testResourceObject_text.text = "";
    }

    public void UpdateUI(UIDisplayItems uiItem, string val)
    {
        switch (uiItem)
        {
            case UIDisplayItems.NULL:
                //do nothing
                break;
            case UIDisplayItems.WOOD:
                woodText.text = val;
                break;
            case UIDisplayItems.STONE:
                stoneText.text = val;
                break;
            default:
                break;
        }

    }

    //test
    public void UpdateResourceText(string val)
    {
        testResourceObject_text.text = val;
    }
}
