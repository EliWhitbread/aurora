﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour, IInteractable
{
    public void HasFocusEnd()
    {
        Debug.Log(gameObject.name + "Has LOST player focus!");
    }

    public void HasFocusStart()
    {
        Debug.Log(gameObject.name + "Has player focus!");
    }

    public void InteractEnd()
    {        
        throw new System.NotImplementedException();
    }

    public void InteractStart(PlayerInteract playerInteracting)
    {
        throw new System.NotImplementedException();
    }
}
