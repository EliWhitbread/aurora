﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RawResourceItem : MonoBehaviour, IDamageable, IInteractable
{
    [SerializeField] HarvestableResourceData resourceData;

    PlayerInteract player;
    Vector3 harvestingForcePosition = Vector3.zero;


    void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += NetworkingClient_EventReceived;
        try
        {
            Rigidbody _rBody = gameObject.GetComponent<Rigidbody>();
            if(_rBody != null)
            {
                _rBody.isKinematic = true;
            }
        }
        catch
        {
            //do nothing... yet!
        }
        
    }

    void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= NetworkingClient_EventReceived;
    }

    private void NetworkingClient_EventReceived(ExitGames.Client.Photon.EventData obj)
    {
        if(obj.Code == 1) //bad! check event enum
        {
            object[] _data = (object[])obj.CustomData;
            int _objID = (int)_data[0];
            harvestingForcePosition = (Vector3)_data[1];
            //int _objID = (int)obj.CustomData;
            //GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIManager>().UpdateResourceText("Event recieved! " + _objID.ToString() + " " + resourceData.id.ToString());
            //Debug.Log("Hello!" + _objID.ToString() + " " + resourceData.id.ToString());
            if (_objID == resourceData.id )
            {
               // GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIManager>().UpdateResourceText("Event recieved! " + _objID.ToString() + " " + resourceData.id.ToString());

               // GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIManager>().UpdateResourceText(this.gameObject.name);
                if(resourceData.resourceType == RawResourceType.WOOD)
                {
                    StartCoroutine(TimedDestroy());
                }
                else
                {
                    this.gameObject.SetActive(false);
                }
                
            }
        }
    }

    IEnumerator TimedDestroy() //only testing trees atm - more to come
    {
        
        Rigidbody _rBody = gameObject.GetComponent<Rigidbody>();
        if (_rBody != null)
        {
            float _size = 5.0f;
            try
            {
                _size = gameObject.transform.GetComponent<Renderer>().bounds.size.y;
            }
            catch
            {
                _size = gameObject.transform.GetChild(0).GetComponent<Renderer>().bounds.size.y;
            }
            
            float _mass = _size * _size;
            _rBody.mass = _mass;
            _rBody.isKinematic = false;
            _rBody.AddForceAtPosition((gameObject.transform.position - harvestingForcePosition).normalized * (_size * 2),
                harvestingForcePosition + Vector3.up, ForceMode.Impulse);
            yield return new WaitForSeconds(10.0f);
        }
        else
        {
            yield return null;
        }
        this.gameObject.SetActive(false);

    }

    //private void PlayerInteract_On_ReturnObjectInstance(int uniqueID)
    //{
    //    if (resourceData.id == uniqueID && gameObject.activeInHierarchy)
    //    {
    //        GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIManager>().UpdateResourceText(this.gameObject.name);
    //        this.gameObject.SetActive(false);
    //    }
    //}

    //private void UpdateEnvironment_On_ReturnInstance(int iID)
    //{
    //    if(resourceData.id == iID && gameObject.activeInHierarchy)
    //    {
    //        GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIManager>().UpdateResourceText(this.gameObject.name);
    //        this.gameObject.SetActive(false);
    //        Debug.Log(resourceData.id.ToString());
    //        //UpdateEnvironment.current.UpdateList(true, this);
    //    }        
    //}

    public HarvestableResourceData ResourceData
    {
        get { return resourceData; }
        set { resourceData = value; }
    }

    public void HasFocusEnd()
    {
        GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIManager>().UpdateResourceText("");

        //if(gameObject.activeInHierarchy)
        //{
        //    UpdateEnvironment.current.UpdateList(false, this);
        //}        
        //Debug.Log(gameObject.name + "Has LOST player focus!");
    }

    public void HasFocusStart()
    {
        //UpdateEnvironment.current.UpdateList(true, this);
        //Debug.Log(gameObject.name + "Has player focus!");
        GameObject.FindGameObjectWithTag("Canvas").GetComponent<UIManager>().UpdateResourceText(resourceData.resourceType.ToString());
    }

    public void InteractEnd()
    {
        //ownershipTransfer.RelinquishOwnership();
    }

    public void InteractStart(PlayerInteract playerInteracting)
    {
        if(player != playerInteracting)
        {
            player = playerInteracting;
        }
        //test
        TakeDamage(10);        
        //pView.RPC("RPC_TakeDamage", RpcTarget.AllBuffered, 10);
    }

    public void TakeDamage(int amt)
    {
        resourceData.durability -= amt;
        if(resourceData.durability <= 0)
        {
            player.InteractionReturn(resourceData, this);

            //UpdateEnvironment.current.RemoveObject(this);

            //pView.RPC("RPC_RemoveResource", RpcTarget.AllBuffered);

            //ownershipTransfer.TransferOwnership();
            //if (pView.IsMine)
            //{
            //    PhotonNetwork.Destroy(this.gameObject);
            //}
            //pView.RPC("RPC_RemoveResource", RpcTarget.AllBuffered);
            //Debug.Log("remove harvestable resource item " + this.gameObject.name + " at: " + this.gameObject.transform.position.ToString());
        }
    }

    //[PunRPC]
    //void RPC_RemoveResource()
    //{
    //    //PhotonNetwork.Destroy(this.gameObject);
    //    pView.ViewID = (int)Random.Range(50, 500);
    //    GameObject.Destroy(PhotonView.Find(pView.ViewID).gameObject);
    //}

    //[PunRPC]
    //void RPC_TakeDamage(int amt)
    //{
    //    //ownershipTransfer.TransferOwnership();

    //    resourceData.durability -= amt;
    //    if (resourceData.durability <= 0)
    //    {
    //        player.InteractionReturn(resourceData);
    //        //this.gameObject.SetActive(false);
    //        PhotonNetwork.Destroy(this.gameObject);
    //    }
    //}
}
