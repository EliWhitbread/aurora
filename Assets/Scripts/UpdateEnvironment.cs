﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class UpdateEnvironment : MonoBehaviourPun
{
    public static UpdateEnvironment current { get; private set; }

    public delegate void ReturnInstance(int uniqueID);
    public static event ReturnInstance On_ReturnInstance;

    [SerializeField] GameObject testObject;
    PhotonView punView;

    //[SerializeField] List<RawResourceItem> interactingWithItems;

    private void Awake()
    {
        if (current != null && current != this)
        {
            Destroy(current.gameObject);
            current = this;
        }
        else
        {
            current = this;
        }
        punView = gameObject.GetComponent<PhotonView>();
    }

    //public void UpdateList(bool add, RawResourceItem rItem)
    //{
    //    if (add == true)
    //    {
    //        AddItem(rItem);
    //        //photonView.RPC("AddItem", RpcTarget.AllBufferedViaServer, rItem);
    //    }
    //    else
    //    {
    //       // RemoveItem(rItem);
    //        //photonView.RPC("RemoveItem", RpcTarget.AllBufferedViaServer, rItem);
    //    }
    //}

    //void AddItem(RawResourceItem rItem)
    //{
    //    if(interactingWithItems == null)
    //    {
    //        interactingWithItems = new List<RawResourceItem>();
    //    }

    //    for (int i = 0; i < interactingWithItems.Count; i++)
    //    {
    //        if(interactingWithItems[i] == rItem)
    //        {
    //            return;
    //        }
    //    }

    //    interactingWithItems.Add(rItem);
    //}    

    //void RemoveItem(RawResourceItem rItem)
    //{
    //    for (int i = 0; i < interactingWithItems.Count; i++)
    //    {
    //        if (interactingWithItems[i] == rItem)
    //        {
    //            interactingWithItems.RemoveAt(i);
    //            return;
    //        }
    //    }
    //}

    public void RemoveObject(RawResourceItem rItem)
    {
       // photonView.RPC("RemoveSceneObject", RpcTarget.AllBufferedViaServer, rItem.ResourceData.id);


        //for (int i = 0; i < interactingWithItems.Count; i++)
        //{
        //    if (interactingWithItems[i] == rItem)
        //    {
        //        photonView.RPC("RemoveSceneObject", RpcTarget.AllBufferedViaServer, i);
        //        return;
        //    }
        //}
    }

    [PunRPC]
    void RemoveSceneObject(int obj)
    {
        if(On_ReturnInstance != null)
        {
            On_ReturnInstance(obj);
        }

        //if (On_ReturnInstance != null)
        //{
        //    On_ReturnInstance(interactingWithItems[obj].gameObject.GetInstanceID());
        //}
        //interactingWithItems[obj].gameObject.SetActive(false);
        //RemoveItem(interactingWithItems[obj]);
    }
}
