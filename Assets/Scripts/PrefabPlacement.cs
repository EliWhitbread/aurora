﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
using System;

public class PrefabPlacement : MonoBehaviour
{
    public GameObject parentToUse;
    public List<GameObject> seedObjects;
    public int maxObjectsToSpawn;
    public float maxSpawnRadius;
    public bool addRandomRotation = false;
    public bool allowXYZRotation = false;
    //[Header("Indentation of prefab into the ground - (tree min should be 0.2)")]
    //public float minGroundIndent;
    //public float maxGroundIndent;
    public float randomScaleFactorMin, randomScaleFactorMax;
    [Header("Colour variation")]
    public String materialPropertyName = "_HueVariation";
    public bool addColourVariation = false;


    //List<GameObject> placedAssets;
    RandomRotationGenerator myRandomGenerator = new RandomRotationGenerator();

    public void PlacePrefabs()
    {

        foreach (GameObject obj in seedObjects)
        {
            for (int i = 0; i < maxObjectsToSpawn; i++)
            {
                Vector3 rayPos = new Vector3(UnityEngine.Random.Range(obj.transform.position.x - maxSpawnRadius, obj.transform.position.x + maxSpawnRadius), obj.transform.position.y + 30.0f, UnityEngine.Random.Range(obj.transform.position.z - maxSpawnRadius, obj.transform.position.z + maxSpawnRadius));

                RaycastHit hit;

                if (Physics.SphereCast(rayPos, 3.0f, Vector3.down, out hit, 35.0f))
                {
                    if (hit.collider.CompareTag("Terrain"))
                    {
#if UNITY_EDITOR
                        var parent = PrefabUtility.GetCorrespondingObjectFromSource(obj);
                        var p = PrefabUtility.InstantiatePrefab(parent as GameObject) as GameObject;

                        float _scaleXZ = UnityEngine.Random.Range(randomScaleFactorMin, randomScaleFactorMax);
                        Vector3 _scale = new Vector3(_scaleXZ, _scaleXZ + UnityEngine.Random.Range(-0.2f, 0.2f), _scaleXZ);
                        p.transform.localScale = _scale;
                        RawResourceItem _rawResourceItem = p.GetComponent<RawResourceItem>();
                        //test
                        HarvestableResourceData _rData = _rawResourceItem.ResourceData;
                        _rData.durability = (int)(80 * _scaleXZ + UnityEngine.Random.Range(-10, 10));
                        _rData.yieldAmount = (int)(30 * _scaleXZ + UnityEngine.Random.Range(-5, 5));
                        _rawResourceItem.ResourceData = _rData;
                        PlaceSpawnedPrefab(p, hit.point);
#endif

                    }
                    else
                    {
                        i--;
                    }
                }
            }
        }


    }
        
    void PlaceSpawnedPrefab(GameObject ob, Vector3 pos)
    {
        if(addColourVariation == true)
        {
            Renderer[] renderers = ob.GetComponentsInChildren<Renderer>();
            float _randColG = UnityEngine.Random.Range(0.1f, 0.9f);
            Color _randCol = new Color(0.0f, _randColG, _randColG + UnityEngine.Random.Range(-0.1f, 0.1f), 0.26f);
            foreach (Renderer rndr in renderers)
            {
                MaterialPropertyBlock _block = new MaterialPropertyBlock();
                rndr.GetPropertyBlock(_block);

                if (_block.GetColor(materialPropertyName) != null)
                {
                    _block.SetColor(materialPropertyName, _randCol);
                }
                else
                {
                    Debug.Log("Can't randomise Colour");
                }

                rndr.SetPropertyBlock(_block);
            }
        }
        
        ob.transform.position = pos;
        if(addRandomRotation == true)
        {
            int randomYRot = myRandomGenerator.RandomRotation();
            if(allowXYZRotation == false)
            {
                ob.transform.rotation = Quaternion.AngleAxis(randomYRot, Vector3.up);
            }
            else
            {
                ob.transform.rotation = Quaternion.Euler(new Vector3(myRandomGenerator.RandomRotation(), randomYRot, myRandomGenerator.RandomRotation()));
            }
            
        }
                
        ob.transform.SetParent(parentToUse.transform);
    }
}

public class RandomRotationGenerator
{
    System.Random rnd;

    public RandomRotationGenerator()
    {
        rnd = new System.Random(Environment.TickCount);
    }

    public int RandomRotation()
    {
        int r = rnd.Next(-180, 180);

        return r;
    }
}