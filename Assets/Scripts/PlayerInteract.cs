﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PlayerInteract : MonoBehaviour
{
    IInteractable interactableObject;
    PlayerInventory playerInventory;

    //public delegate void ReturnObjectInstance(int uniqueID);
    //public static event ReturnObjectInstance On_ReturnObjectInstance;

    //pun
    PhotonView punView;

    //pun events
    private readonly byte RemoveObjectFromSceneEvent = 1;

    public IInteractable InteractableObject
    {
        get { return interactableObject; }
        set { interactableObject = value; }
    }

    private void Awake()
    {
        playerInventory = gameObject.GetComponent<PlayerInventory>();
        punView = gameObject.GetComponent<PhotonView>();
    }

    private void Start()
    {
        InputManager.OnMouseInputBegin += InputManager_OnMouseInputBegin;
        InputManager.OnMouseInputEnd += InputManager_OnMouseInputEnd;
    }

    private void OnDisable()
    {
        InputManager.OnMouseInputBegin -= InputManager_OnMouseInputBegin;
        InputManager.OnMouseInputEnd -= InputManager_OnMouseInputEnd;
    }

    private void InputManager_OnMouseInputBegin(MouseInput mouseInput)
    {
        if(interactableObject == null)
        {
            return;
        }
        interactableObject.InteractStart(this);
    }

    private void InputManager_OnMouseInputEnd(MouseInput mouseInput)
    {
        if (interactableObject == null)
        {
            return;
        }
    }

    public void InteractionReturn(HarvestableResourceData resourceData, RawResourceItem item)
    {

        if (punView.IsMine)
        {
            object[] _data = new object[] { resourceData.id, this.gameObject.transform.position };
            Photon.Realtime.RaiseEventOptions _options = new Photon.Realtime.RaiseEventOptions { CachingOption = Photon.Realtime.EventCaching.AddToRoomCacheGlobal,
                Receivers = Photon.Realtime.ReceiverGroup.All};
            PhotonNetwork.RaiseEvent(RemoveObjectFromSceneEvent, _data, _options , ExitGames.Client.Photon.SendOptions.SendReliable);
            Debug.Log("Good bye! " + _data.ToString());

            //punView.RPC("SendObjectDestroyCall", RpcTarget.MasterClient, resourceData.id);
            //UpdateEnvironment.current.UpdateList(true, item);
            //UpdateEnvironment.current.RemoveObject(item);
        }

        switch (resourceData.resourceType)
        {
            case RawResourceType.NULL:
                break;
            case RawResourceType.WOOD:
                playerInventory.AvailableWood += resourceData.yieldAmount;
                break;
            case RawResourceType.STONE:
                playerInventory.AvailableStone += resourceData.yieldAmount;
                break;
            default:
                break;
        }
    }

    //[PunRPC]
    //void SendObjectDestroyCall(int id)
    //{
    //    if (On_ReturnObjectInstance != null)
    //    {
    //        On_ReturnObjectInstance(id);
    //    }
    //}

}
