﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct HarvestableResourceData
{
    public int id;
    public RawResourceType resourceType;
    public int durability, yieldAmount;
}