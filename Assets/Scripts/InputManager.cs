﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public delegate void PlayerMouseInput(MouseInput mouseInput);
    public static event PlayerMouseInput OnMouseInputBegin;
    public static event PlayerMouseInput OnMouseInputEnd;

    public delegate void PlayerKeyInput(KeyboardInput keyboardInput);
    public static event PlayerKeyInput OnKeyDown_Begin;
    public static event PlayerKeyInput OnKeyUp;

    public delegate void PlayerMovementInput(Vector2 val);
    public static event PlayerMovementInput OnMovementInput;

    Vector2 moveInputVector = Vector2.zero;
    bool moveInputDetected = false;

    void Update()
    {
        //movement
        moveInputVector.x = Input.GetAxis("Horizontal");
        moveInputVector.y = Input.GetAxis("Vertical");

        if (moveInputDetected == true)
        {
            if (OnMovementInput != null)
            {
                OnMovementInput(moveInputVector);
            }
            moveInputDetected = false;
        }
        else if(moveInputVector != Vector2.zero)
        {
            moveInputDetected = true;
        }

        //interaction
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            if (OnMouseInputBegin != null)
            {
                OnMouseInputBegin(MouseInput.MOUSE_LEFT);
            }
        }
        if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            if (OnMouseInputEnd != null)
            {
                OnMouseInputEnd(MouseInput.MOUSE_LEFT);
            }
        }
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (OnKeyDown_Begin != null)
            {
                OnKeyDown_Begin(KeyboardInput.ESC);
            }
            if (Cursor.visible == false)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

        }
        if (Input.GetKeyDown(KeyCode.F10))
        {
            Screen.fullScreen = !Screen.fullScreen;
        }

        //Settings
        if(Input.GetKeyDown(KeyCode.BackQuote))
        {
            if(OnKeyDown_Begin != null)
            {
                OnKeyDown_Begin(KeyboardInput.CAMERA_VIEW_SWITCH);
            }
        }
    }
}
