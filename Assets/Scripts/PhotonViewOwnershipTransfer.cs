﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;

public class PhotonViewOwnershipTransfer : MonoBehaviourPun
{
    public void TransferOwnership()
    {
        base.photonView.RequestOwnership();
    }

    public void RelinquishOwnership()
    {
        if(base.photonView.IsMine)
        {
            base.photonView.TransferOwnership(0);
        }
    }
}
